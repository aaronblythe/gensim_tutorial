from memory_profiler import memory_usage
from time import sleep
 
# Example 1, list comprehension. No streaming.
# First create an array of squares, then sum it.
# Note the inner array is simply looped over: no random access, just iteration.
# Wasteful, isn't it?
@profile
def fnostream():
    numbers = range(100000)
    thesum = sum([n**2 for n in numbers])
    sleep(.1)
    return thesum
 
# Generator: square and sum one value after another
# No extra array created = lazily evaluated stream of numbers!
@profile
def fyesstream():
    numbers = range(100000)
    thesum = sum(n**2 for n in numbers)
    sleep(.1)
    return thesum

if __name__ == '__main__':
    fnostream()
    fyesstream()
