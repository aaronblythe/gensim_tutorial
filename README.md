# Background

Gensim for Data Streaming

Gensim [1] is a popular python library for the NLP and IR community that is used at many companies, mainly for unsupervised document analysis.

From the Gensim Survey in April 2018 [2]:

"I was surprised that data streaming was important to only 17% of respondents. This tells me that people either train their unsupervised models on small in-memory datasets (really?), or we failed to communicate streaming and generators properly. Is streaming and online learning too advanced, do developers know how to do it? If puzzled, read our data streaming tutorial. [3] We need better docs."

# Purpose

To evaluate the docs on data streaming and possibly offer something that is easier to follow.



