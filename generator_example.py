generator = (word + '!' for word in 'baby let me iterate ya'.split())
# The generator object is now created, ready to be iterated over.
# No exclamation marks added yet at this point.
 
for val in generator: # real processing happens here, during iteration
    print(val, end=" ")
#baby! let! me! iterate! ya!
 
for val in generator:
    print(val, end=" ")
# Nothing printed! No more data, generator stream already exhausted above.